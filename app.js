const express = require('express')
const morgan = require('morgan')
const app = express()
const enquete = require('./enquete.json')
const app = require('./src/serveur')


app.use(morgan('dev'))

app.listen(8080, () => {
  console.log('Serveur à lecoute')
})

app.get('/', function (req,res) {
    res.send('voici le get')
})

app.get('/info', function (req,res) {
    res.send('jsau-apiserver-1.0.0')
})

app.post('/info', function (req,res) {
    res.send('jsau-apiserver-1.0.0')
})

app.put('/', function (req, res) {
  res.send('Got a PUT request at /user')
})

app.delete('/', function (req, res) {
  res.send('Got a DELETE request at /user')
})
/ GET //enquete
app.get('/enquete', (req,res) => {
    res.status(200).json(enquete)
})

// GET /enquete/:id
app.get('/enquete/:id', (req,res) => {
    const id = parseInt(req.params.id)
    const personne = enquete.find(personne=> personne.id === id)
    res.status(200).json(personne|| null)
})

// POST /enquete
app.post('/enquete', (req,res) => {
    enquete.push(req.body)
    res.status(200).json(enquete)
})

// PUT /enquete/:id
app.put('/enquete/:id', (req,res) => {
    const id = parseInt(req.params.id)
    let personne = enquete.find(personne=> personne.id === id)
    personne.Prenom =req.body.Prenom,
    personne.Nom =req.body.Nom,
    personne.age =req.body.age,
    res.status(200).json(personne)
})

// DELETE /enquete/:id
app.delete('/enquete/:id', (req,res) => {
    const id = parseInt(req.params.id)
    let personne = enquete.find(personne => personne.id === id)
    enquete.splice(enquete.indexOf(personne),1)
    res.status(200).json(enquete)
})
