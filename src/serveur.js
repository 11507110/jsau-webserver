'use strict'
const express = require('express')
const bodyParser = require('body-parser')

const nunjucks = require('nunjucks')
const path = require('path')

const app = express()
const fs = require('fs')

nunjucks.configure('src/vues', {
    autoescape:  true,
    express:  app
})


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
require('./app.js')(app, fs)

app.use(express.static(path.join(__dirname, 'views')))

module.exports = app
